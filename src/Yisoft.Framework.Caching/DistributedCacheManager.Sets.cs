//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.Text;
using System.Threading.Tasks;

namespace Yisoft.Framework.Caching
{
    public partial class DistributedCacheManager
    {
        public void Set(string key, bool value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = BitConverter.GetBytes(value);

            Set(key, data, absoluteExpirationRelativeToNow, slidingExpiration);
        }

        public Task SetAsync(string key, bool value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = BitConverter.GetBytes(value);

            return SetAsync(key, data, absoluteExpirationRelativeToNow, slidingExpiration);
        }

        public void Set(string key, byte value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = new[] {value};

            Set(key, data, absoluteExpirationRelativeToNow, slidingExpiration);
        }

        public Task SetAsync(string key, byte value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = new[] {value};

            return SetAsync(key, data, absoluteExpirationRelativeToNow, slidingExpiration);
        }

        public void Set(string key, char value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = BitConverter.GetBytes(value);

            Set(key, data, absoluteExpirationRelativeToNow, slidingExpiration);
        }

        public Task SetAsync(string key, char value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = BitConverter.GetBytes(value);

            return SetAsync(key, data, absoluteExpirationRelativeToNow, slidingExpiration);
        }

        public void Set(string key, double value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = BitConverter.GetBytes(value);

            Set(key, data, absoluteExpirationRelativeToNow, slidingExpiration);
        }

        public Task SetAsync(string key, double value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = BitConverter.GetBytes(value);

            return SetAsync(key, data, absoluteExpirationRelativeToNow, slidingExpiration);
        }

        public void Set(string key, short value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = BitConverter.GetBytes(value);

            Set(key, data, absoluteExpirationRelativeToNow, slidingExpiration);
        }

        public Task SetAsync(string key, short value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = BitConverter.GetBytes(value);

            return SetAsync(key, data, absoluteExpirationRelativeToNow, slidingExpiration);
        }

        public void Set(string key, int value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = BitConverter.GetBytes(value);

            Set(key, data, absoluteExpirationRelativeToNow, slidingExpiration);
        }

        public Task SetAsync(string key, int value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = BitConverter.GetBytes(value);

            return SetAsync(key, data, absoluteExpirationRelativeToNow, slidingExpiration);
        }

        public void Set(string key, long value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = BitConverter.GetBytes(value);

            Set(key, data, absoluteExpirationRelativeToNow, slidingExpiration);
        }

        public Task SetAsync(string key, long value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = BitConverter.GetBytes(value);

            return SetAsync(key, data, absoluteExpirationRelativeToNow, slidingExpiration);
        }

        public void Set(string key, float value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = BitConverter.GetBytes(value);

            Set(key, data, absoluteExpirationRelativeToNow, slidingExpiration);
        }

        public Task SetAsync(string key, float value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = BitConverter.GetBytes(value);

            return SetAsync(key, data, absoluteExpirationRelativeToNow, slidingExpiration);
        }

        public void Set(string key, ushort value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = BitConverter.GetBytes(value);

            Set(key, data, absoluteExpirationRelativeToNow, slidingExpiration);
        }

        public Task SetAsync(string key, ushort value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = BitConverter.GetBytes(value);

            return SetAsync(key, data, absoluteExpirationRelativeToNow, slidingExpiration);
        }

        public void Set(string key, uint value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = BitConverter.GetBytes(value);

            Set(key, data, absoluteExpirationRelativeToNow, slidingExpiration);
        }

        public Task SetAsync(string key, uint value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = BitConverter.GetBytes(value);

            return SetAsync(key, data, absoluteExpirationRelativeToNow, slidingExpiration);
        }

        public void Set(string key, ulong value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = BitConverter.GetBytes(value);

            Set(key, data, absoluteExpirationRelativeToNow, slidingExpiration);
        }

        public Task SetAsync(string key, ulong value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = BitConverter.GetBytes(value);

            return SetAsync(key, data, absoluteExpirationRelativeToNow, slidingExpiration);
        }

        public void Set(string key, DateTime value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            Set(key, value.Ticks, absoluteExpirationRelativeToNow, slidingExpiration);
        }

        public Task SetAsync(string key, DateTime value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            return SetAsync(key, value.Ticks, absoluteExpirationRelativeToNow, slidingExpiration);
        }

        public void Set(string key, string value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));
            if (value == null) throw new ArgumentNullException(nameof(value));

            var data = Encoding.UTF8.GetBytes(value);

            Set(key, data, absoluteExpirationRelativeToNow, slidingExpiration);
        }

        public Task SetAsync(string key, string value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));
            if (value == null) throw new ArgumentNullException(nameof(value));

            var data = Encoding.UTF8.GetBytes(value);

            return SetAsync(key, data, absoluteExpirationRelativeToNow, slidingExpiration);
        }
    }
}
