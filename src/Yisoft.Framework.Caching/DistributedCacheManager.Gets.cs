//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.Text;
using System.Threading.Tasks;

namespace Yisoft.Framework.Caching
{
    public partial class DistributedCacheManager
    {
        public bool? GetBoolean(string key, bool? defaultValue = null)
        {
            var data = Get(key);

            return data == null ? defaultValue : BitConverter.ToBoolean(data, 0);
        }

        public async Task<bool?> GetBooleanAsync(string key, bool? defaultValue = null)
        {
            var data = await GetAsync(key);

            return data == null ? defaultValue : BitConverter.ToBoolean(data, 0);
        }

        public byte? GetByte(string key, byte? defaultValue = null)
        {
            var data = Get(key);

            return data?[0] ?? defaultValue;
        }

        public async Task<byte?> GetByteAsync(string key, byte? defaultValue = null)
        {
            var data = await GetAsync(key);

            return data?[0] ?? defaultValue;
        }

        public char? GetChar(string key, char? defaultValue = null)
        {
            var data = Get(key);

            return data == null ? defaultValue : BitConverter.ToChar(data, 0);
        }

        public async Task<char?> GetCharAsync(string key, char? defaultValue = null)
        {
            var data = await GetAsync(key);

            return data == null ? defaultValue : BitConverter.ToChar(data, 0);
        }

        public DateTime? GetDateTime(string key, DateTime? defaultValue = null)
        {
            var ticks = GetInt64(key);

            return ticks == null ? defaultValue : new DateTime(ticks.Value);
        }

        public async Task<DateTime?> GetDateTimeAsync(string key, DateTime? defaultValue = null)
        {
            var ticks = await GetInt64Async(key);

            return ticks == null ? defaultValue : new DateTime(ticks.Value);
        }

        public double? GetDouble(string key, double? defaultValue = null)
        {
            var data = Get(key);

            return data == null ? defaultValue : BitConverter.ToDouble(data, 0);
        }

        public async Task<double?> GetDoubleAsync(string key, double? defaultValue = null)
        {
            var data = await GetAsync(key);

            return data == null ? defaultValue : BitConverter.ToDouble(data, 0);
        }

        public float? GetSingle(string key, float? defaultValue = null)
        {
            var data = Get(key);

            return data == null ? defaultValue : BitConverter.ToSingle(data, 0);
        }

        public async Task<float?> GetSingleAsync(string key, float? defaultValue = null)
        {
            var data = await GetAsync(key);

            return data == null ? defaultValue : BitConverter.ToSingle(data, 0);
        }

        public short? GetInt16(string key, short? defaultValue = null)
        {
            var data = Get(key);

            return data == null ? defaultValue : BitConverter.ToInt16(data, 0);
        }

        public async Task<short?> GetInt16Async(string key, short? defaultValue = null)
        {
            var data = await GetAsync(key);

            return data == null ? defaultValue : BitConverter.ToInt16(data, 0);
        }

        public int? GetInt32(string key, int? defaultValue = null)
        {
            var data = Get(key);

            return data == null ? defaultValue : BitConverter.ToInt32(data, 0);
        }

        public async Task<int?> GetInt32Async(string key, int? defaultValue = null)
        {
            var data = await GetAsync(key);

            return data == null ? defaultValue : BitConverter.ToInt32(data, 0);
        }

        public long? GetInt64(string key, long? defaultValue = null)
        {
            var data = Get(key);

            return data == null ? defaultValue : BitConverter.ToInt64(data, 0);
        }

        public async Task<long?> GetInt64Async(string key, long? defaultValue = null)
        {
            var data = await GetAsync(key);

            return data == null ? defaultValue : BitConverter.ToInt64(data, 0);
        }

        public ushort? GetUInt16(string key, ushort? defaultValue = null)
        {
            var data = Get(key);

            return data == null ? defaultValue : BitConverter.ToUInt16(data, 0);
        }

        public async Task<ushort?> GetUInt16Async(string key, ushort? defaultValue = null)
        {
            var data = await GetAsync(key);

            return data == null ? defaultValue : BitConverter.ToUInt16(data, 0);
        }

        public uint? GetUInt32(string key, uint? defaultValue = null)
        {
            var data = Get(key);

            return data == null ? defaultValue : BitConverter.ToUInt32(data, 0);
        }

        public async Task<uint?> GetUInt32Async(string key, uint? defaultValue = null)
        {
            var data = await GetAsync(key);

            return data == null ? defaultValue : BitConverter.ToUInt32(data, 0);
        }

        public ulong? GetUInt64(string key, ulong? defaultValue = null)
        {
            var data = Get(key);

            return data == null ? defaultValue : BitConverter.ToUInt64(data, 0);
        }

        public async Task<ulong?> GetUInt64Async(string key, ulong? defaultValue = null)
        {
            var data = await GetAsync(key);

            return data == null ? defaultValue : BitConverter.ToUInt64(data, 0);
        }

        public string GetString(string key, string defaultValue = null)
        {
            var data = Get(key);

            return data == null ? defaultValue : Encoding.UTF8.GetString(data, 0, data.Length);
        }

        public async Task<string> GetStringAsync(string key, string defaultValue = null)
        {
            var data = await GetAsync(key);

            return data == null ? defaultValue : Encoding.UTF8.GetString(data, 0, data.Length);
        }
    }
}
