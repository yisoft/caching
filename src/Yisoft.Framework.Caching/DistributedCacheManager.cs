//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;

namespace Yisoft.Framework.Caching
{
    public partial class DistributedCacheManager
    {
        private readonly IDistributedCache _distributedCache;

        public DistributedCacheManager(IDistributedCache distributedCache) { _distributedCache = distributedCache; }

        private static DistributedCacheEntryOptions _createCacheEntryOptions(TimeSpan? absoluteExpirationRelativeToNow, TimeSpan? slidingExpiration)
        {
            return new DistributedCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = absoluteExpirationRelativeToNow,
                SlidingExpiration = slidingExpiration
            };
        }

        public byte[] Get(string key) { return _distributedCache.Get(key); }

        public Task<byte[]> GetAsync(string key) { return _distributedCache.GetAsync(key); }

        public void Refresh(string key) { _distributedCache.Refresh(key); }

        public Task RefreshAsync(string key) { return _distributedCache.RefreshAsync(key); }

        public void Remove(string key) { _distributedCache.Remove(key); }

        public Task RemoveAsync(string key) { return _distributedCache.RemoveAsync(key); }

        public void Set(string key, byte[] value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var options = _createCacheEntryOptions(absoluteExpirationRelativeToNow, slidingExpiration);

            _distributedCache.Set(key, value, options);
        }

        public Task SetAsync(string key, byte[] value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var options = _createCacheEntryOptions(absoluteExpirationRelativeToNow, slidingExpiration);

            return _distributedCache.SetAsync(key, value, options);
        }
    }
}
