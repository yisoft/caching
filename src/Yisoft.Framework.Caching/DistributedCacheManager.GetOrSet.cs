//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.Threading.Tasks;

namespace Yisoft.Framework.Caching
{
    public partial class DistributedCacheManager
    {
        public bool? Get(string key, Func<bool> dataFunc, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = GetBoolean(key);

            if (data != null) return data;

            var x = dataFunc();

            Set(key, x, absoluteExpirationRelativeToNow, slidingExpiration);

            return x;
        }

        public async Task<bool?> GetAsync(string key, Func<bool> dataFunc, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = await GetBooleanAsync(key);

            if (data != null) return data;

            var x = dataFunc();

            await SetAsync(key, x, absoluteExpirationRelativeToNow, slidingExpiration);

            return x;
        }

        public byte? Get(string key, Func<byte> dataFunc, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = GetByte(key);

            if (data != null) return data;

            var x = dataFunc();

            Set(key, x, absoluteExpirationRelativeToNow, slidingExpiration);

            return x;
        }

        public async Task<byte?> GetAsync(string key, Func<byte> dataFunc, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = await GetByteAsync(key);

            if (data != null) return data;

            var x = dataFunc();

            await SetAsync(key, x, absoluteExpirationRelativeToNow, slidingExpiration);

            return x;
        }

        public char? Get(string key, Func<char> dataFunc, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = GetChar(key);

            if (data != null) return data;

            var x = dataFunc();

            Set(key, x, absoluteExpirationRelativeToNow, slidingExpiration);

            return x;
        }

        public async Task<char?> GetAsync(string key, Func<char> dataFunc, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = await GetCharAsync(key);

            if (data != null) return data;

            var x = dataFunc();

            await SetAsync(key, x, absoluteExpirationRelativeToNow, slidingExpiration);

            return x;
        }

        public DateTime? Get(string key, Func<DateTime> dataFunc, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = GetDateTime(key);

            if (data != null) return data;

            var x = dataFunc();

            Set(key, x, absoluteExpirationRelativeToNow, slidingExpiration);

            return x;
        }

        public async Task<DateTime?> GetAsync(string key, Func<DateTime> dataFunc, TimeSpan? absoluteExpirationRelativeToNow = null,
            TimeSpan? slidingExpiration = null)
        {
            var data = await GetDateTimeAsync(key);

            if (data != null) return data;

            var x = dataFunc();

            await SetAsync(key, x, absoluteExpirationRelativeToNow, slidingExpiration);

            return x;
        }

        public double? Get(string key, Func<double> dataFunc, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = GetDouble(key);

            if (data != null) return data;

            var x = dataFunc();

            Set(key, x, absoluteExpirationRelativeToNow, slidingExpiration);

            return x;
        }

        public async Task<double?> GetAsync(string key, Func<double> dataFunc, TimeSpan? absoluteExpirationRelativeToNow = null,
            TimeSpan? slidingExpiration = null)
        {
            var data = await GetDoubleAsync(key);

            if (data != null) return data;

            var x = dataFunc();

            await SetAsync(key, x, absoluteExpirationRelativeToNow, slidingExpiration);

            return x;
        }

        public float? Get(string key, Func<float> dataFunc, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = GetSingle(key);

            if (data != null) return data;

            var x = dataFunc();

            Set(key, x, absoluteExpirationRelativeToNow, slidingExpiration);

            return x;
        }

        public async Task<float?> GetAsync(string key, Func<float> dataFunc, TimeSpan? absoluteExpirationRelativeToNow = null,
            TimeSpan? slidingExpiration = null)
        {
            var data = await GetSingleAsync(key);

            if (data != null) return data;

            var x = dataFunc();

            await SetAsync(key, x, absoluteExpirationRelativeToNow, slidingExpiration);

            return x;
        }

        public short? Get(string key, Func<short> dataFunc, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = GetInt16(key);

            if (data != null) return data;

            var x = dataFunc();

            Set(key, x, absoluteExpirationRelativeToNow, slidingExpiration);

            return x;
        }

        public async Task<short?> GetAsync(string key, Func<short> dataFunc, TimeSpan? absoluteExpirationRelativeToNow = null,
            TimeSpan? slidingExpiration = null)
        {
            var data = await GetInt16Async(key);

            if (data != null) return data;

            var x = dataFunc();

            await SetAsync(key, x, absoluteExpirationRelativeToNow, slidingExpiration);

            return x;
        }

        public int? Get(string key, Func<int> dataFunc, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = GetInt32(key);

            if (data != null) return data;

            var x = dataFunc();

            Set(key, x, absoluteExpirationRelativeToNow, slidingExpiration);

            return x;
        }

        public async Task<int?> GetAsync(string key, Func<int> dataFunc, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = await GetInt32Async(key);

            if (data != null) return data;

            var x = dataFunc();

            await SetAsync(key, x, absoluteExpirationRelativeToNow, slidingExpiration);

            return x;
        }

        public long? Get(string key, Func<long> dataFunc, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = GetInt64(key);

            if (data != null) return data;

            var x = dataFunc();

            Set(key, x, absoluteExpirationRelativeToNow, slidingExpiration);

            return x;
        }

        public async Task<long?> GetAsync(string key, Func<long> dataFunc, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = await GetInt64Async(key);

            if (data != null) return data;

            var x = dataFunc();

            await SetAsync(key, x, absoluteExpirationRelativeToNow, slidingExpiration);

            return x;
        }

        public ushort? Get(string key, Func<ushort> dataFunc, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = GetUInt16(key);

            if (data != null) return data;

            var x = dataFunc();

            Set(key, x, absoluteExpirationRelativeToNow, slidingExpiration);

            return x;
        }

        public async Task<ushort?> GetAsync(string key, Func<ushort> dataFunc, TimeSpan? absoluteExpirationRelativeToNow = null,
            TimeSpan? slidingExpiration = null)
        {
            var data = await GetUInt16Async(key);

            if (data != null) return data;

            var x = dataFunc();

            await SetAsync(key, x, absoluteExpirationRelativeToNow, slidingExpiration);

            return x;
        }

        public uint? Get(string key, Func<uint> dataFunc, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = GetUInt32(key);

            if (data != null) return data;

            var x = dataFunc();

            Set(key, x, absoluteExpirationRelativeToNow, slidingExpiration);

            return x;
        }

        public async Task<uint?> GetAsync(string key, Func<uint> dataFunc, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = await GetUInt32Async(key);

            if (data != null) return data;

            var x = dataFunc();

            await SetAsync(key, x, absoluteExpirationRelativeToNow, slidingExpiration);

            return x;
        }

        public ulong? Get(string key, Func<ulong> dataFunc, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = GetUInt64(key);

            if (data != null) return data;

            var x = dataFunc();

            Set(key, x, absoluteExpirationRelativeToNow, slidingExpiration);

            return x;
        }

        public async Task<ulong?> GetAsync(string key, Func<ulong> dataFunc, TimeSpan? absoluteExpirationRelativeToNow = null,
            TimeSpan? slidingExpiration = null)
        {
            var data = await GetUInt64Async(key);

            if (data != null) return data;

            var x = dataFunc();

            await SetAsync(key, x, absoluteExpirationRelativeToNow, slidingExpiration);

            return x;
        }

        public string Get(string key, Func<string> dataFunc, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = GetString(key);

            if (data != null) return data;

            data = dataFunc();

            Set(key, data, absoluteExpirationRelativeToNow, slidingExpiration);

            return data;
        }

        public async Task<string> GetAsync(string key, Func<string> dataFunc, TimeSpan? absoluteExpirationRelativeToNow = null,
            TimeSpan? slidingExpiration = null)
        {
            var data = await GetStringAsync(key);

            if (data != null) return data;

            data = dataFunc?.Invoke();

            await SetAsync(key, data, absoluteExpirationRelativeToNow, slidingExpiration);

            return data;
        }
    }
}
