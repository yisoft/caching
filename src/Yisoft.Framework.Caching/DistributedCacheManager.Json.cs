//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;

namespace Yisoft.Framework.Caching
{
    public partial class DistributedCacheManager
    {
        public T FromJson<T>(string key, T defaultValue = default(T))
        {
            var data = Get(key);

            return data == null ? defaultValue : _Deserialize<T>(data);
        }

        public async Task<T> FromJsonAsync<T>(string key, T defaultValue = default(T))
        {
            var data = await GetAsync(key);

            return data == null ? defaultValue : _Deserialize<T>(data);
        }

        public T FromJson<T>(string key, Func<T> dataFunc, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            var data = FromJson<T>(key);

            if (data != null) return data;

            var x = dataFunc();

            if (x == null) return default(T);

            SetAsJson(key, x, absoluteExpirationRelativeToNow, slidingExpiration);

            return x;
        }

        public async Task<T> FromJsonAsync<T>(string key, Func<Task<T>> dataFunc, TimeSpan? absoluteExpirationRelativeToNow = null,
            TimeSpan? slidingExpiration = null)
        {
            var data = await FromJsonAsync<T>(key);

            if (data != null) return data;

            var x = await dataFunc();

            if (x == null) return default(T);

            await SetAsJsonAsync(key, x, absoluteExpirationRelativeToNow, slidingExpiration);

            return x;
        }

        public void SetAsJson<T>(string key, T value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));
            if (value == null) throw new ArgumentNullException(nameof(value));

            var data = _Serialize(value);

            Set(key, data, absoluteExpirationRelativeToNow, slidingExpiration);
        }

        public Task SetAsJsonAsync<T>(string key, T value, TimeSpan? absoluteExpirationRelativeToNow = null, TimeSpan? slidingExpiration = null)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));
            if (value == null) throw new ArgumentNullException(nameof(value));

            var data = _Serialize(value);

            return SetAsync(key, data, absoluteExpirationRelativeToNow, slidingExpiration);
        }

        private static byte[] _Serialize<T>(T value)
        {
            using (var stream = new MemoryStream())
            {
                using (var writer = new BsonDataWriter(stream))
                {
                    var serializer = new JsonSerializer();

                    serializer.Serialize(writer, value);
                }

                return stream.ToArray();
            }
        }

        private static T _Deserialize<T>(byte[] data)
        {
            using (var stream = new MemoryStream(data))
            {
                using (var reader = new BsonDataReader(stream))
                {
                    var serializer = new JsonSerializer();

                    try
                    {
                        return serializer.Deserialize<T>(reader);
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e);
                        Debug.WriteLine(reader.ReadAsString());

                        throw;
                    }
                }
            }
        }
    }
}
