﻿# Yisoft.Framework.Caching
扩展缓存操作，提供一组更加实用的缓存操作方式。

停止更新，推荐使用[MichaCo/CacheManager](https://github.com/MichaCo/CacheManager)。

# License
Released under the [MIT license](http://creativecommons.org/licenses/MIT/).